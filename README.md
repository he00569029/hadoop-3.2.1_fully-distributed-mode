# Hadoop Ecosystem

## Introduction

This project is mainly to understand the application of hadoop ecosystem.
The ecosystem is include:

- Fully-distributed mode of Hadoop 3.2.1 version.
- HBase
- Hive
- Zookeeper
- Spark

### Installation

The hadoop ecosystem mentioned above has documents.
Preparation before setting up the environment:

- Virtual Box

- Ubuntu 20.04 + Guest Addition

The installation steps are not certain, but you must install hadoop first. Because all of ecosystem is based on hadoop.
Document order for installing hadoop is **Hadoop_install.md** and then Fully_Distributed.md

### Operation

The project here is to connect the various systems in series, and it is also the main goal of my training.
Goal of training include

- Operation HDFS system.
- Import HDFS data to HBase.
- Use Hive to ETL.
- Import ETL data to python or Pyspark.
