# Advance Operation

## Load Data

### From HDFS

- Edit hadoop-env.sh

Add following line at bottom.

```xml
export HADOOP_CLASSPATH=/usr/local/hbase/lib/*
```

- Upload data to HDFS

```shell
$hadoop fs -put {local data path} {hdfs path}
```

- Generate HFile
  - Start HBase

    ```shell
    $cd /usr/local/hbase/bin
    $./start-hbase.sh
    ```

  - Create Hbase Table

    Start Hbase Shell

    ```shell
    $cd /usr/local/hbase/bin
    $./hbase shell
    ```

    Use shell to create table

    ```shell
    hbase(main):> create '<table_name>','<column_family>'
    ```

  - Use Bulkload to Generate HFile

    ```shell
    $cd /usr/local/hbase/bin/
    $hadoop jar /usr/local/hbase/lib/hbase-mapreduce-2.2.5.jar importtsv \
    -Dimporttsv.columns=HBASE_ROW_KEY,columnfamily:col_A,columnfamily:col_B \
    -Dimporttsv.separator=',' \
    -Dimporttsv.bulk.output=hdfs://master:9000/usr/local/bulkload/result \
    {HBASE Table_name} \
    {HDFS_csv_file_path}
    ```

    Check Bulkload Path in HDFS

    ```shell
    $hadoop dfs -ls /usr/local/bulkload/result
    ```

    You can see two file in Bulkload folder in HDFS
    1. _SUCCESS
    2. columnfamily

- Load Data into HBase From HDFS

```shell
$cd /usr/local/hbase/bin/
$./hbase org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles \
{HDFS_HFile_path} \
{HBase_Table_name}
```
