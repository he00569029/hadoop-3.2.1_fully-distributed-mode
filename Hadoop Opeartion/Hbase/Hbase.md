# Hbase

## Basis Operation

- Create Table

```hbase
hbase(main):> create '<table_name>','<column_family>'
```

- Check Table

```hbase
hbase(main):> list
```

- Check Table Enable

```hbase
hbase(main):> is_enabled '<table_name>'
```

It will return True or False

- Enable & Disable Table

```hbase
hbase(main):> enable '<table_name>'
hbase(main):> is_enabled '<table_name>'
```

Check table should be enable

```hbase
hbase(main):> disable '<table_name>'
hbase(main):> is_enabled '<table_name>'
```

Check table should be disable

- Describe Table

```hbase
hbase(main):> describe '<table_name>'
```

<!-- - Alter Table

```hbase
hbase(main):> alter '<table_name>'
``` -->

- Put Data into Table

```hbase
hbase(main):> put '<table_name>','rowid','<colfamily:colname>','<value>'
```

use scan to show the data

```hbase
hbase(main):> scan '<table_name>'
```

- Update Value Of Table

```hbase
hbase(main):>  put '<table_name>','rowid','<colfamily:colname>','<new_value>'
```

- Read Data From Table
  - Read Specific Row Data

    ```hbase
    hbase(main):> get '<table_name>','rowid'
    ```

  - Read Specific Column Data

    ```hbase
    hbase(main):> get '<table_name>','rowid', {COLUMN=> 'colfamily:colname'}
    ```

- Delete Table
  - Delete Specific Column

    ```hbase
    hbase(main):> delete '<table_name>','rowid','colfamily:colname','time stamp'
    ```

    **Only delete value of the last update.**

  - Delete Specific Row

    ```hbase
    hbase(main):> deleteall '<table_name>','rowid'
    ```

- Count Table

```hbase
hbase(main):> count '<table_name>'
```

- Truncate Table

```hbase
hbase(main):> truncate '<table_name>'
```
