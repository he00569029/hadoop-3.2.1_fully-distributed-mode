# PyHive Operation

## Connection Hive use python

- Import package

```python
from pyhive import
import pandas as pd
```

- Hive connect

```python
conn = hive.Connection(host="your_host_ip", port=10000, username="your_username" ,auth='NOSASL')
cursor = conn.cursor()
```

- Use query to get data of table

```python
select_stmt = 'SELECT * FROM <database>.<table>'
cursor.execute(select_stmt)
```

- Transform dataframe

```python
df = pd.DataFrame( cursor.fetchall() )
```

- Get column name use Pyhive

```python
select_stmt = 'describe <database>.<table>'
cursor.execute(select_stmt)
column_list = list(cursor.fetchall())
```

---
**Now you can do ETL in Hive through python.**
