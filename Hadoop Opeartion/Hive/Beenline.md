# Beeline Operation

## Pre-work

- Modify hive-site.xml
  
  ```shell
  $sudo gedit /usr/local/hive/conf/hive-site.xml
  ```

  Modify the value of hive.server2.authentication to "NOSASL".

- Start hiveserver2

  ```shell
  $cd /usr/local/hive/bin
  $hiveserver2
  ```

### Beeline

Open another termianal and switch to /hive/bin/ path.
Use following command to start beeline.

```shell
$beeline
```

- Connect HIVE database

```shell
beeline> !connect jdbc:hive2://{master_ip}:10000/{database_name};auth=noSasl
```

>Now we can use the query to get the more beautiful results.

- Use beeline to query data

```shell
0: jdbc:hive2://> select * from train limit 15;
```
