# Hive Operation

## Pre-work

- In this section, use kaggle's titanic dataset.
- Data has been stored in HDFS.
- HDFS path is /usr/local/dataset/titanic/. Threre are three files in this folder.

### Load data to Hive from HDFS (Internal table)

- Start Hive
  
  ```shell
  $cd /usr/local/hadoop/sbin/
  $start-all.sh
  $cd /usr/local/hive/bin/
  $hive
  ```

- Create database in Hive
  
  ```shell
  hive> create database titanic;
  ```

- Create internal table and defined column

  ```shell
  hive> create table titanic.train (PassengerId int, Survived int, Pclass int, Last_name string, Name string, Sex string, Age float, SibSp int, Parch int, Ticket string, Fare float, Cabin string, Embarked string)
  hive> ROW FORMAT DELIMITED
  hive> FIELDS TERMINATED BY ','
  hive> STORED AS TEXTFILE;
  ```

- Load data into Hive

  ```shell
  hive> LOAD DATA INPATH "hdfs://master:9000/usr/local/dataset/titanic/train.csv" INTO TABLE titanic.train;
  ```

- Check HDFS location.

  ```shell
  $hadoop fs -ls /usr/local/dataset/titanic/
  ```

  >_This moment, we found train.csv wil disappear.
The reason is the file will put into hive's metastore(manager store) when we create internal table not external table._

- Check hive folder in HDFS.

  ```shell
  $hadoop fs -ls /usr/local/hive/warehouse/titanic.db/
  ```

  **We found train.csv be moved into titanic.db folder.**

### Load data to Hive from HDFS (External table)

- Create external folder in hdfs and put the file.

  ```shell
  $hadoop fs -mkdir /usr/local/dataset/titanic/external
  $hadoop fs -put /home/he00569029/dataset/titanic/train.csv /usr/local/dataset/titanic/external/
  ```

- Check the file had put in external folder

  ```shell
  $hadoop fs -ls /usr/local/dataset/titanic/external/
  ```

- Create external table

  ```shell
  hive> use titanic;
  hive> create external table titanic.new_train(PassengerId int, Survived int, Pclass int, Name string, Sex string, Age int, SibSp int, Parch int, Ticket string, Fare float, Cabin string, Embarked string)
  hive> ROW FORMAT DELIMITED
  hive> FIELDS TERMINATED BY ','
  hive> STORED AS TEXTFILE
  hive> LOCATION 'hdfs://master:9000/usr/local/dataset/titanic/external/'
  hive> tblproperties('skip.header.line.count'='1');
  ```

**Notice:**

1. Now we can obersved that file in hdfs external folder still exist.
Because the external hasn't put the file to Hive metastore, and doesn't move the hdfs file to warehouse folder in hdfs.
2. The locatoin direction a "folder" not a file. If folder has multiple data, the table will combine a table, so i don't recommend that folder has multiple file.

#### Query

- Normal query
  
  ```shell
  hive> select * from titanic.train;
  ```

  ```shell
  hive> create table selction_table as
  hive> select passengerid,sex,age,survived from train;
  ```

#### Other operation

- Create internal table
  
  ```shell
  hive> create table <database>.<table_name> (column_1 type, column_2 type)
  hive> ROW FORMAT DELIMITED
  hive> FIELDS TERMINATED BY ','
  hive> STORED AS TEXTFILE;
  ```

- Create external table
  
  ```shell
  hive> create external table <database>.<table_name> (column_1 type, column_2 type)
  hive> ROW FORMAT DELIMITED
  hive> FIELDS TERMINATED BY ','
  hive> STORED AS TEXTFILE;
  ```

- Check table is internal or external.

  ```shell
  hive> describe formatted <table_name>;
  ```

  >Then check the data_type is external or managed_table

- Change the external table to internal table.

  ```shell
  hive> alter table <table_name> set tblproperties('EXTERNAL'='FALSE');
  ```

  >Check the data_type is transformed to managed_table.

- Move table to another database.

  ```shell
  hive> alter table <table_name> rename to <new_database>.<table_name>;
  ```
