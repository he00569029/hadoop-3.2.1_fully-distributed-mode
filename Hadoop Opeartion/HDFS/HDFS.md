# HDFS Operation

## Import local data to HDFS

- Start hadoop
  
  ```shell
  $cd /usr/local/hadoop/sbin
  $start-all.sh
  ```

- Create directory in HDFS
  
  ```shell
  $hadoop fs -mkdir /usr/local/dataset
  ```

- Check new folder should be created
  
  ```shell
  $hadoop fs -ls /usr/local/
  ```

- Put local data into HDFS folder
  - In this section, we use kaggle API to download titanic dataset

    ```shell
    $sudo mkdir dataset
    $kaggle competitions download -c titanic
    $sudo tar xzf titanic
    $sudo mv titanic /home/{username}/dataset/
    ```

  - Put titanic dataset into HDFS

    ```shell
    $hadoop fs -put /home/{username}/dataset/titanic/ /usr/local/dataset/
    ```

- Verify the file in HDFS folder

    ```shell
    $hadoop fs -ls /usr/local/dataset/
    ```

## Retrieving data from HDFS

- View the data

    ```shell
    $hadoop fs -cat /usr/local/dataset/titanic/train.csv
    ```
