# Sqoop Operation

>Change the bin directory in sqoop before execute sqoop.

#### Check all the databases

```shell
$./sqoop list-databaes \
--connect jdbc:mysql://{DBMS_hostname}:3306/ \
--username {username} \
--password {password}
```

#### Check all the tables in database

```shell
$./sqoop list-tables \
--connect jdbc:mysql://{DBMS_hostname}:3306/{database_name} \
--username {username} \
--password {password}
```

#### Export hdfs data to mysql

```shell
$./sqoop export --connect jdbc:mysql://{DBMS_hostname}:3306/{database_name} \
--username {username in mysql} \
--password {password in mysql} \
--table {DBMS_tablename} \
--export-dir {hdfs_file_path+dataname} \
--input-fields-terminated-by ',' \
--columns="{table_column_name}"
--m 1
```

**Notice:**

- In mysql should had created database and table before doing this command.
- The most import thing is check the data format in table and hdfs file, these condition must be met before it can be successfully export.

#### Import data from Mysql to HIVE

1. Create a **external** table in hive **without** in any database.

2. Use the following sqoop command.

    ```shell
    ./sqoop import \
    --connect jdbc:mysql://{DBMS_hostname}:3306/{database_name} \
    --username {database_username} \
    --password {database_password} \
    --table {database_table} \
    --fields-terminated-by ',' \
    --hive-import \
    --direct \
    --hive-table {hive_table_name} \
    --null-string '' \
    --null-non-string '' \
    -m 1 \
    --warehouse-dir /usr/local/hive/warehouse/
    ```

**Notice:**

- Import table is a **external** table.
- Create a external table before use sqoop.
- Can't create table in any database, because sqoop can't capture hive database.
- Warehouse-dir is a mysql connect to hive's dir, it's not a internal table or csv file, for me it's check data is should import to this hdfs path.
