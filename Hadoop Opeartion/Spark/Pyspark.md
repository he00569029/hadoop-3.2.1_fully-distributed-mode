# PySpark

## Load file

- Load data from local file

```python
# Import packages
from pyspark import SparkContext, SparkConf
# Connect cluster
sc = SparkContext(master='local',appName='test')
# Load data from local file
textFile = sc.textFile('file:///home/he00569029/dataset/titanic/train.csv')
```

![load data from local file](pyspark_photo/load%20data%20from%20local%20file.jpg)

- Load data from HDFS

```python
# Load data from HDFS
textFile_hdfs = sc.textFile('hdfs://master:9000/usr/local/dataset/titanic/train.csv')
```

![load data from hdfs](pyspark_photo/load%20data%20from%20hdfs.jpg)

- Load data from HIVE

```python
# Import packages
from pyspark.sql import SparkSession, HiveContext , SQLContext
# Load data from HIVE
spark = SparkSession \
        .builder \
        .appName("Python Spark SQL Hive integration example") \
        .config("spark.sql.warehouse.dir", 'hdfs://master:9000/usr/local/hive/warehouse/') \
        .enableHiveSupport() \
        .getOrCreate()

spark.sql('SHOW DATABASES').show()
```

![load data from hive select databases](pyspark_photo/sql%20database.jpg)

```python
spark.sql('SHOW TABLES').show()
```

![load data from hive select tables](pyspark_photo/sql%20show%20tables.jpg)

## RDDs Operation

>In this section, we use **textFile** for RDD command operation

### Transform

> Transform include Map,FlatMap,Filter,Distinct etc..

#### Map

```python
## each item do the following action
new_text = textFile.map(lambda x: "ABC"+x )
print(new_text.first())
```

![map new_text](pyspark_photo/map_new_text_first.jpg)

```python
print(textFile.first())
```

![map textfile first](pyspark_photo/map_textfile_first.jpg)

```python
## use map to select specific column
textFile.map(lambda x:x.split(',')[0:2])
```

![map select specific column](pyspark_photo/map_select_specific_column.jpg)

#### FlatMap

```python
## each row use split function to get the every char
new_text = textFile.flatMap(lambda line: line.split(','))
print(new_text.first())
```

![flatmap](pyspark_photo/flatmap.jpg)

#### Filter

```python
# create arrayrdd
arrayrdd = sc.parallelize([1,2,3,4,5])
print(arrayrdd.count())
```

![filter arrayrdd](pyspark_photo/filter_arrayrdd_count.jpg)

```python
arrayrdd02 = arrayrdd.filter(lambda x: x%2 ==0)
print( arrayrdd02.collect() )
```

![filter arrayrdd](pyspark_photo/filter_arrayrdd02_collect.jpg)

```python
## Add index to all the RDD
new_text = textFile.zipWithIndex()
## filter the index <=5 (include index)
print(new_text.filter(lambda x:x[1]<=5).collect() )
```

![filter select index <=5 include index](pyspark_photo/filter_index_include.jpg)

```python
## use map to get the value of index (non include index)
print(new_text.filter(lambda x: x[1]<=5).map(lambda x: x[0]).collect() )
```

![filter select index<=5 non index](pyspark_photo/filter_index_non.jpg)

```python
## ignore header to get the remain value of index (non include index)
new_text.filter(lambda x:x[1]>0).map(lambda x: x[0]).first()
```

![filter ignore header](pyspark_photo/filter_ignore_header.jpg)

#### Distinct

```python
## drop duplicate
arrayrdd = sc.parallelize([1,2,3,4,5,2,4,5])
array_dist = arrayrdd.distinct()
array_dist.count()
array_dist.collect()
```

![distinct](pyspark_photo/distinct.jpg)

#### Sample

```python
arrayrdd = sc.parallelize([1,2,27,4,5,56,7,86,11,35])
# fraction is the probability of each sample being drawn is 0.5
sample_set = arrayrdd.sample(withReplacement=False , fraction=0.5)
sample_set.collect()
```

![sample](pyspark_photo/sample.jpg)

#### Sortbykey

```python
data = textFile.zipWithIndex().filter(lambda x:x[1]>0).map(lambda x:x[0])
# Only capture PassengerId , Survived,Pclass
data_split = data.map(lambda x: x.split(',')[0:3])
data_split.take(5)
```

![sortbykey data_split](pyspark_photo/sortbykey_data_split_take5.jpg)

```python
#Let all element switch, correspond Pclass , Survived , PassengerId
data_switch = data_split.map(lambda x:((x[2],x[1]),x[0]) )
data_switch.take(5)
```

![sortbykey data_switch](pyspark_photo/sortbykey_data_switch_take5.jpg)

```python
# The key is x[2] then x[1] then x[0]
data_sort = data_switch.sortByKey().map(lambda x:(x[0][0],x[0][1],x[1]))
#flat each tuple
data_sort.take(5)
```

![sortbykey data_sort](pyspark_photo/sortbykey_data_sort_take5.jpg)

#### Groupbykey

```python
# get the Survived and Pclass
new_text = textFile.map(lambda x:x.split(',')[1:3] )
# get the value of index ,ignore header and index
new_text = new_text.zipWithIndex().filter(lambda x:x[1]>0).map(lambda x:x[0])
new_text.take(5)
```

![groupbykey new_text](pyspark_photo/groupbykey_new_text_take5.jpg)

```python
# use first value be the keys, second value be the values
group_text = new_text.groupByKey()
group_text.map(lambda x:(x[0] ,list(x[1]))).foreach(print)
```

![groupbykey group_text](pyspark_photo/groupbykey_group_text.print.jpg)

#### Reducebykey

```python
#get the Survived and Pclass
new_text = textFile.map(lambda x:x.split(',')[1:3] )
# get the value of index ,ignore header and index
new_text = new_text.zipWithIndex().filter(lambda x:x[1]>0).map(lambda x:x[0])
new_text.take(5)
```

![reducebykey new_text](pyspark_photo/reducebykey_new_text_take5.jpg)

```python
group_text = new_text.reduceByKey(lambda x,y:x+y)
group_text.foreach(print)
```

![reducebykey group_text](pyspark_photo/groupbykey_group_text.print.jpg)

### Action

#### Takesample

```python
# take number sample of rdd
arrayrdd = sc.parallelize([1,2,27,4,5,56,7,86,11,35])
takesample_set = arrayrdd.takeSample(withReplacement=False, num=5 )
print(takesample_set)
```

![takesample](pyspark_photo/takesample.jpg)

#### Reduce

```python
arrayrdd = sc.parallelize([1,2,27,4,5,56,7,86,11,35])
arrayrdd.reduce(lambda x,y:x-y)
```

![reduce](pyspark_photo/reduce.jpg)

>**first,count,collect,take(n),foreach(function) all of them are RDDs action.**
