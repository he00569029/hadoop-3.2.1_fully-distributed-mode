# Pyspark DataFrame

>Continue the previous section, we still use **textFile** to operation DataFrame command.

## Load data from local file

```scala
// import packages
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession, HiveContext , SQLContext
from pyspark.sql.types import DoubleType, IntegerType, StringType, StructField, StructType

// Connect cluster
sc = SparkContext(master='local',appName='test')
// Load data from local file
textFile = sc.textFile('file:///home/he00569029/dataset/titanic/train.csv')
```

## Convert RDD to Dataframe

```scala
// Defined column name and type
df_schema = StructType([
        StructField(name='PassengerId',dataType=StringType()),
        StructField(name='Survived',dataType=StringType()),
        StructField(name='Pclass',dataType=StringType()),
        StructField(name='Last_name',dataType=StringType()),
        StructField(name='Name',dataType=StringType()),
        StructField(name='Sex',dataType=StringType()),
        StructField(name='Age',dataType=StringType()),
        StructField(name='SibSp',dataType=StringType()),
        StructField(name='Parch',dataType=StringType()),
        StructField(name='Ticket',dataType=StringType()),
        StructField(name='Fare',dataType=StringType()),
        StructField(name='Cabin',dataType=StringType()),
        StructField(name='Embarked',dataType=StringType())
])

spark = SparkSession.builder \
        .appName('AppName') \
        .config('spark.master','local')\
        .getOrCreate()

data = textFile.zipWithIndex().filter(lambda x: x[1]>0).map(lambda x:x[0])
data_split = data.map(lambda x:x.split(','))
df = spark.createDataFrame(data=data_split,schema=df_schema)
df.show()
```

![df show](pyspark_dataframe_photo/df_show.jpg)

---

## DataFrame Base Operation

- Head
  
```scala
// head 5 rows
df.head(5)
```

![df head](pyspark_dataframe_photo/df_head5.jpg)

- Tail

```scala
// tail 5 rows
df.tail(5)
```

![df tail](pyspark_dataframe_photo/df_tail5.jpg)

- Count

```scala
// Check n rows of data
df.count()
```

![df count](pyspark_dataframe_photo/df_count.jpg)

- Column Name

```scala
// Check column name
df.columns
```

![df column](pyspark_dataframe_photo/df_columns.jpg)

- Column Type

```scala
// Check column type
df.dtypes
```

![df type](pyspark_dataframe_photo/df_dtypes.jpg)

- Describe

```scala
// Summary data of column
df.describe('Pclass').show()
```

![df describe](pyspark_dataframe_photo/df_describe.jpg)

---

## DataFrame Advanced Operation

- Filter data

```scala
// use filter to select data of Survived value is 0
df.filter(df.Survived == 0).show()
```

![df filter](pyspark_dataframe_photo/df_filter_survived0.jpg)

- Select specific column

```scala
// use select to show the specific column
df.select('Cabin').show()
```

![df select specific column](pyspark_dataframe_photo/df_select_specific_column.jpg)

- Select multiple column

```scala
// select multiple column
df.select(['Name','Survived']).show()
```

![df select multiple column](pyspark_dataframe_photo/select_multiple_column.jpg)

- Distinct

```scala
// select Embarked column and count the unique value
df.select('Embarked').distinct().count()
```

![df distinct](pyspark_dataframe_photo/select_column_distinct_count.jpg)

- Change column type

```scala
// Change column type
df = df.withColumn('Survived',df.Survived.cast(DoubleType()))
df.dtypes
```

![df change column type](pyspark_dataframe_photo/change_column_type.jpg)

- Add new column

```scala
// Add new column and give the value
new_df = df.withColumn('new_col',df.PassengerId*1)
new_df.show()
```

![df add new column](pyspark_dataframe_photo/new_df_add_column.jpg)

- Rename column

```scala
// Rename column name
new_df = new_df.withColumnRenamed('new_col','copy_col')
new_df.show()
```

![df rename column](pyspark_dataframe_photo/new_df_rename_column_name.jpg)

- Join

```scala
// create new_df, have Survived and Name column. select Survived is 0, and first 5 instance.
new_df = df.select('Survived','Name').filter(df.Survived==0).limit(5)
new_df.show()
```

![join new_df](pyspark_dataframe_photo/join_new_df.jpg)

```scala
// use join to select data, find new_df data indexed by Name
test = df.join(new_df,'Name','inner')
test.show()
```

![join test](pyspark_dataframe_photo/join_test.jpg)

- Groupby

```scala
test.groupBy('Name').count().show()
```

![groupby](pyspark_dataframe_photo/groupby.jpg)
