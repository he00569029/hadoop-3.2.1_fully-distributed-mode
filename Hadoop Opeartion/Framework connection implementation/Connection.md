# Framework connection implementation

## Architecture diagram

- Local file -> HDFS
- HDFS -> Mysql
- Mysql -> HIVE
- HIVE -> Pyspark

### Local file import to HDFS

- Download house price dataset in kaggle.

```shell
$sudo mkdir /home/{username}/dataset/house
$kaggle competitions download -c house-prices-advanced-regression-techniques
$sudo unzip house-prices-advanced-regression-techniques.zip -d /home/{username}/dataset/house/
```

- Start hadoop

```shell
$cd /usr/local/hadoop/sbin/
$start-all.sh
```

![start-all.sh](photo/hadoop.jpg)

- Put local file to HDFS

```shell
$hadoop fs -put /home/{username}/dataset/house /usr/local/dataset/
```

Check data in HDFS

```shell
$hadoop fs -ls /usr/local/dataset/house/
$hadoop fs -cat /usr/local/dataset/house/train.csv
```

![hdfs](photo/hadoop_ls.jpg)

### Import HDFS data to Mysql

- Create database and table

```sql
$mysql -u {user} -p
mysql> create database hdfs_file;
mysql> use hdfs_file;
mysql> create table house_train (Id int, .... );
```

- Remove header of train.csv, upload to HDFS

```shell
$cd /home/{username}/dataset/house
$sed '1d' train.csv > train_new.csv
$hadoop fs -put /home/{username}/dataset/house/train_new.csv /usr/local/dataset/house/
```

![hdfs_train_new](photo/train_new_in_hdfs.jpg)

- Import data to mysql from HDFS

```shell
$cd /usr/local/sqoop/bin/
$./sqoop export \
--connect jdbc:mysql://{host_ip}:3306/hdfs_file \
--username {username} \
--password {password} \
--table house_train \
--export-dir /usr/local/dataset/house/train_new.csv \
-m 1
```

![sqoop_export](photo/sqoop_export.jpg)

- Query table with mysql

```sql
mysql> select * from house_train;
```

![mysql_select](photo/mysql_housetrain.jpg)

### Load data to HIVE from Mysql

- Create external table in Hive

```shell
$cd /usr/local/hive/bin/
$hive
hive> create external table house_train(column1 type, column2 type,...)
hive> row format delimited
hive> fields terminated by ','
hive> stored as textfile;
hive> show tables;
```

- Import data to hive from mysql

```shell
$./sqoop import \
--connect jdbc:mysql://{host_ip}:3306/hdfs_file \
--username {username} \
--password {password} \
--table house_train \
--fields-terminated-by ',' \
--hive-import \
--direct \
--hive-table house_train \
--null-string '' \
--null-non-string '' \
-m 1 \
--warehouse-dir /usr/local/hive/tmp/
```

![mysql_import_hive](photo/mysql_import_hive.jpg)

- Query external table

```shell
hive> select * from house_train;
```

![hive_tables](photo/hive_select.jpg)

- Transform and move into database
  - Create hive database

    ```shell
    hive> create database hive;
    ```

  - Transform external table to internal table

    ```shell
    hive> alter table house_train set tblproperties('EXTERNAL'='FALSE');
    hive> describe formatted house_train;
    ```

    ![hive_table_formatted](photo/house_formatted.jpg)

  - Move table

    ```shell
    hive> alter table house_train rename to hive.house;
    hive> use hive;
    hive> show tables;
    ```

    ![beeline_table](photo/beeline_hive.jpg)

#### Load data to Spark from Hive

>The necessary setting is that the setting of HIVE must be **Remote Mode.**

Now open the Visual Studio Code, then create a python file.

```python
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession, HiveContext , SQLContext

spark = SparkSession \
        .builder \
        .appName("Python Spark SQL Hive integration example") \
        .config("spark.sql.warehouse.dir", 'hdfs://master:9000/usr/local/hive/warehouse/') \
        .enableHiveSupport() \
        .getOrCreate()

spark.sql('SHOW DATABASES').show()

spark.sql('USE HIVE')
spark.sql('SHOW TABLES').show()
spark.sql('SELECT * FROM house').show()
```

Pyspark show databases:

![pyspark_databases](photo/pyspark_databases.jpg)

Pyspark show tables in HIVE database:
![pyspark_tables](photo/pyspark_tables.jpg)

Pyspark using query:
![pyspark_select](photo/pyspark_select.jpg)
