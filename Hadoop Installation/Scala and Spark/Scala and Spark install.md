# Scala and Spark Installation

## Scala Install Step

- Download Scala
  
    ```shell
    $sudo wget https://github.com/scala/scala/archive/v2.13.3.tar.gz
    ```

- Tar file
  
    ```shell
    $sudo tar xzf v2.13.3.tar.gz
    ```

- Move and Rename directory
  
    ```shell
    $sudo mv scala-2.13.3 scala
    $sudo mv scala /usr/local/
    ```

- Modify bashrc file
  
    ```shell
    $sudo gedit /home/{username}/.bashrc
    ```

    Add following lines

    ```xml
    #Set SCALA_HOME
    export SCALA_HOME=/usr/local/scala
    export PATH=$PATH:$SCALA_HOME/bin
    ```

- Application bashrc file

    ```shell
    $source ~/.bashrc
    ```

- Check PATH

    ```shell
    $echo $SCALA_HOME
    $echo $PATH
    ```

## Spark Install Step

- Download Spark

    ```shell
    $sudo wget https://downloads.apache.org/spark/spark-3.0.0/spark-3.0.0-bin-hadoop3.2.tgz
    ```

- Tar file

    ```shell
    $sudo tar xzf spark-3.0.0-bin-hadoop3.2.tgz
    ```

- Move and Rename directory

    ```shell
    $sudo mv spark-3.0.0-bin-hadoop3.2 spark
    $sudo mv spark /usr/local/
    ```

- Modify bashrc file

    ```shell
    $sudo gedit /home/{username}/.bashrc
    ```

    Add following lines

    ```xml
    #Set SPARK_HOME
    export SPARK_HOME=/usr/local/spark
    export PATH=$PATH:$SPARK_HOME/bin
    ```

- Application bashrc file

    ```shell
    $source ~/.bashrc
    ```

- Check PATH

    ```shell
    $echo $SPARK_HOME
    $echo $PATH
    ```

## Start Spark

```shell
$spark-shell
```

If commnad line start title is scala, you had already installed spark.
