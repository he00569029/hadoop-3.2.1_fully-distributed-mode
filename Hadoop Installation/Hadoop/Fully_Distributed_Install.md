# Fully-Distributed Install

## 大綱

- 設定Hadoop的設定檔
  - 編輯hadoop-env.sh
  - 編輯hostname、hosts
  - 編輯HDFS設定檔
    - core-site.xml
    - hdfs-site.xml
    - yarn-site.xml
    - mapred-site.xml
  - 設置namenode、datanode的位置
  - 編輯workers

- Slave製作及修改
  - slave製作
  - 連線測試
  - ssh設定

- 啟動hadoop
  - 格式化HDFS
  - 啟動Namenode、Datanode
  - 啟動NodeManager、ResourceManager
  - 查看Hadoop進度

### 安裝開始

---

- hadoop設定檔
  - 編輯hadoop-env.sh

    ```shell
    $sudo gedit /usr/local/hadoop/etc/hadoop/hadoop-env.sh
    ```

    在最下面加入以下內容

    ```xml
    export PDSH_RCMD_TYPE=ssh
    ```

  - 編輯hostname、hosts

    hostname是指向此機台名稱的，所以更改為master

    ```shell
    $sudo gedit /etc/hostname
    ```

    輸入以下字樣

    ```xml
    master
    ```

    hosts是指向所有工作的機台，所以在IPV6上方的空白處加入以下字句

    ```shell
    $sudo gedit /etc/hosts
    ```

    輸入以下字樣

    ```xml
    IPV4位置  master
    IPV4位置  slave01
    IPV4位置  slave02
    ```

    >若不曉得機台的IPV4位置，請至畫面右上方的電源鍵按下去並進去設定/網路，找192.168開頭的位置並記下來，通常slave機台的位址會往後多1

  - 編輯HDFS檔案
    - core-site.xml

        ```shell
        $sudo gedit $HADOOP_HOME/etc/hadoop/core-site.xml
        ```

        在最下方的configuration內加入以下內容，為一般性的設定

        ```xml
        <property>
            <name>fs.defaultFS</name>
            <value>hdfs://master:9000</value>
        </property>
        <property>
            <name>fs.defaultFS.name</name>
            <value>hdfs://master:9000</value>
        </property>
        <property>
            <name>hadoop.tmp.dir</name>
            <value>file:/usr/local/hadoop/tmp</value>
            <description>Abase for other temporary directories.</description>
        </property>
        ```

    - hdfs-site.xml

        ```shell
        $sudo gedit $HADOOP_HOME/etc/hadoop/hdfs-site.xml
        ```

        在configuration內加入以下內容，設定Namenode、Datanode

        ```xml
        <!-- hdfs的slave機台數量 若有兩台(slave01與slave02)  就將value設為2 -->
        <property>
            <name>dfs.replication</name>
            <value>2</value>
        </property>
        <!-- namenode的儲存位置 -->
        <property>
            <name>dfs.namenode.name.dir</name>
            <value>file:/usr/local/hadoop/hdfs/namenode</value>
        </property>
        <!-- datanode的儲存位置 -->
        <property>
            <name>dfs.datanode.data.dir</name>
            <value>file:/usr/local/hadoop/hdfs/datanode</value>
        </property>
        <property>
            <name>dfs.permissions</name>
            <value>false</value>
        </property>
        ```

    - yarn-site.xml

        ```shell
        $sudo gedit $HADOOP_HOME/etc/hadoop/yarn-site.xml
        ```

        在configuration裡面加入以下內容，設定ResourceManager、NodeManager

        ```xml
        <property>
            <name>yarn.nodemanager.aux-services</name>
            <value>mapreduce_shuffle</value>
        </property>
        <property>
            <name>yarn.resourcemanager.hostname</name>
            <value>master</value>
        </property>
        <property>
            <name>yarn.nodemanager.env-whitelist</name>
            <value>JAVA_HOME,HADOOP_COMMON_HOME,HADOOP_HDFS_HOME,HADOOP_CONF_DIR,CLASSPATH_PREPEND_DISTCACHE,HADOOP_YARN_HOME,HADOOP_MAPRED_HOME</value>
        </property>
        ```

    - mapred-site.xml

        ```shell
        $sudo gedit $HADOOP_HOME/etc/hadoop/mapred-site.xml
        ```

        在configuration裡面，新增以下內容，設定MapReduce程式

        ```xml
        <property>
            <name>mapreduce.framework.name</name>
            <value>yarn</value>
        </property>
        <property>
            <name>mapreduce.application.classpath</name>
            <value>$HADOOP_MAPRED_HOME/share/hadoop/mapreduce/*:$HADOOP_MAPRED_HOME/share/hadoop/mapreduce/lib/*</value>
        </property>
        ```

  - 設置namenode和datanode的位置

    ```shell
    $mkdir /usr/local/hadoop/hdfs
    $mkdir /usr/local/hadoop/hdfs/datanode
    $mkdir /usr/local/hadoop/hdfs/namenode
    ```

  - 編輯workers

    ```shell
    $sudo gedit /usr/local/hadoop/etc/hadoop/workers
    ```

    此檔案主要是指向datanode的機台，所以只需要新增slave機台的名稱就好

    ```xml
    slave01
    slave02
    etc
    ```

- slave機台製作及修改
  - slave製作
    - 關閉master機台，對著master右鍵選取再製。
    - 機台名稱取名為slave01及slave02，選取存放路徑，MAC位址原則選擇「為所有網路卡產生新的MAC位址」，下一步選取「完整再製」

  - 連線測試
    - 開啟master及slave01機台
    - 修改hostname(slave01機台上)
        將master變更為slave01並將機台重開

        ```shell
        $sudo gedit /etc/hostname
        $sudo reboot
        ```

    - 確認連線狀態(slave01機台上)
    - 在slave01機台上確認IPV4位址，若跟先前打得有所不同，請更改hosts
    - 打開terminal，打上以下內容確認能與master做連線

        ```shell
        ping 192.168.xx.xxx  (master的IPV4位址)
        ```

    - 換master機台操作，並ping slave01機台，確認是否能夠連線
    **Slave02機台重複上面步驟(更改hostname、ping機台)**
  - ssh設定
    在master機台上把slave ssh key 建立出來並測試

    ```shell
    $cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
    $scp -r ~/.ssh slave01:~/
    $scp -r ~/.ssh slave02:~/
    ```

    測試ssh進入slave01並且不需要打密碼(在master機台上)

    ```shell
    ssh slave01
    exit
    ```

    若不需要打密碼就代表成功了，好了之後打上exit退出
    **slave02機台一樣要測試ssh**

- 啟動hadoop

  - 格式化HDFS (On Master)
    >**只有第一次執行需要進行格式化**

    ```shell
    $hdfs namenode -format
    ```

  - 啟動Namenode、Datanode (On Master)

    ```shell
    $cd $HADOOP_HOME/sbin
    $./start-dfs.sh
    ```

    啟動完之後在master機台上輸入jps會看到以下功能

    ```shell
    Namenode
    jps
    SecondaryNameNode
    ```

    另開一個terminal，輸入ssh slave01，並輸入jps會看到以下功能

    ```shell
    jps
    Datanode
    ```

  - 啟動NodeManager、ResourceManager

    ```shell
    $start-yarn.sh
    ```

    - master機台上輸入jps會看到多了**ResourceManager**
    - slave機台上輸入jps會看到多了**NodeManager**

  - 查看Hadoop進度
    打開瀏覽器，在網址列打上

    ```shell
    localhost:9870
    ```

    - 若有看到綠色的Hadoop頁面就代表有成功啟動namenode、datanode、hadoop
    - 在Datanode頁面上也會有slave01機台正在啟動

    在網址列打上

    ```shell
    master:8088/cluster
    ```

    - 若有網頁顯示，就代表成功啟動ResourceManage、NodeManager
