# Hadoop 安裝筆記

## 安裝環境

- Ubuntu 20.04 + VirtualBox + Guest Addition
- Apache Hadoop  3.2.1

## 事前準備

- 裝好ubuntu 20.04 + Guest Addition
- 機器的網路配置，介面卡1選「NAT」，介面卡2選「僅限主機介面卡」

## 安裝步驟

---

- 安裝JAVA，有openjdk跟oraclejdk，只安裝openjdk
  
    ```shell
    $sudo apt-get install openjdk-8-jdk
    $java -version
    $javac -version
    ```

    >_查看版本  要第 8 版_

- 安裝ssh、pdsh
  
    ```shell
    $sudo apt-get install ssh
    $sudo apt-get install pdsh
    $sudo apt-get install openssh-server openssh-client
    $ssh-keygen -t rsa -P ""
    $cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys
    $ssh localhost
    ```

    >_要確定不用打密碼就可以登入_

- 下載hadoop
  
    ```shell
    $wget https://downloads.apache.org/hadoop/common/hadoop-3.2.1/hadoop-3.2.1.tar.gz
    ```

- 解壓縮hadoop
    將hadoop-3.1.2資料夾移動到/user/local/資料夾內，並將資料夾名稱名稱改為hadoop

    ```shell
    $tar xzf hadoop-3.2.1.tar.gz
    $sudo mv hadoop-3.2.1 hadoop
    $sudo mv hadoop /usr/local/
    ```

- 設定 JAVA_HOME 跟 hadoop_HOME變數
    了解java的路徑在哪，查看Best那一行，路徑取到java版本就好

    ```shell
    $update-alternatives --query java
    $sudo gedit /home/{userid}/.bashrc
    ```

    >另一種方法是，在桌面點個人資料夾，開啟隱藏檔案，對著bashrc檔右鍵編輯

   打開bashrc檔後，在最下方加入以下內容

    ```xml
    # Set HADOOP_HOME
    export HADOOP_HOME=/usr/local/hadoop
    # Set JAVA_HOME
    export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
    export PATH=${JAVA_HOME}/bin:$PATH
    # Set HBase
    export HBASE_HOME=/usr/local/hbase
    export HADOOP_INSTALL=$HADOOP_HOME
    export HADOOP_MAPRED_HOME=$HADOOP_HOME
    export HADOOP_COMMON_HOME=$HADOOP_HOME
    export HADOOP_HDFS_HOME=$HADOOP_HOME
    export YARN_HOME=$HADOOP_HOME
    export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
    export HADOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib/native"
    #Add Hadoop bin and sbin directory to PATH
    export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$HABSE_HOME/bin
    export HADOOP_CLASSPATH=$JAVA_HOME/lib/tools.jar
    export JAVA_LIBRARY_PATH=$HADOOP_HOME/lib/native:JAVA_LIBRARY_PATH
    ```

    應用環境變數

    ```shell
    $source ~/.bashrc
    ```

    在terminal裡，輸入以下內容，查看是否都有設定到

    ```shell
    echo $JAVA_HOME
    echo $PATH
    ```

- 設定hadoop-env.sh中的JAVA_HOME變數

    ```shell
    $sudo gedit /usr/local/hadoop/etc/hadoop/hadoop-env.sh
    ```

    搜尋export JAVA_HOME，把後面資料補齊，變成以下內容
    _**(注意)要把前面的#給取消掉**_

    ```xml
    export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
    ```

- 執行測試

    ```shell
    $mkdir input
    $cp $HADOOP_HOME/etc/hadoop/*.xml input
    $hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.2.1.jar grep input output 'dfs[a-z.]+'
    $cat output/*
    ```

    如果有跑出dfsadmin就代表有運作

## 總結

---
其實安裝Hadoop的Single Node模式並不困難，照官方文件也能實做得出來。
若要將Hadoop設定為完全分散式模式，請見Fully_Distributed_Install.md。
