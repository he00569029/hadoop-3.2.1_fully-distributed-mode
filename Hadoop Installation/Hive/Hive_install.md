# Hive Installation Step

## Installation Type

- Embedded Mode
- Local Mode
- Remote Mode

In this install section, we divided into two way.
>**We use embedded mode first, and then use remote mode.**

### Hive Install Step (Embedded Mode)

- Download Hive

```shell
$sudo wget https://downloads.apache.org/hive/hive-3.1.2/apache-hive-3.1.2-bin.tar.gz
```

- Tar file

```shell
$sudo tar xzf apache-hive-3.1.2-bin.tar.gz
```

- Move and rename file

```shell
$sudo mv apache-hive-3.1.2-bin hive
$sudo mv hive /usr/local/
```

- Modify bashrc file

```shell
$sudo gedit /home/{username}/.bashrc
```

Add following lines at bottom.

```xml
#Set HIVE_HOME
export HIVE_HOME=/usr/local/hive
export PATH=$PATH:$HIVE_HOME/bin
```

- Application bashrc file

```shell
$cd /home/{username}/
$source ~/.bashrc
```

- Check PATH
  
```shell
$echo $HIVE_HOME
$echo $PATH
```

## Hive Configuration

- Create two separate directories to store data in the HDFS layer:

  1. The temporary, tmp directory is going to store the intermediate results of Hive processes.
  2. The warehouse directory is going to store the Hive related tables.

    **But, we have to start hadoop before do these command.**

    ```shell
    $cd /usr/local/hadoop/sbin/
    $start-all.sh
    ```

    Now we can enter the following command.

    ```shell
    $hdfs dfs -mkdir -p /usr/local/hive/warehouse
    $hdfs dfs -mkdir -p /usr/local/hive/tmp
    $hdfs dfs -chmod g+w /usr/local/hive/warehouse
    $hdfs dfs -chmod g+w /usr/local/hive/tmp
    ```

- Modify hive-env.sh

```shell
$cd /usr/local/hive/conf
$sudo cp hive-env.sh.template hive-env.sh
$sudo chmod +x hive-env.sh
$sudo gedit hive-env.sh
```

Add following lines at bottom.

```xml
export HADOOP_HOME=/usr/local/hadoop
export HIVE_CONF_DIR=/usr/local/hive/conf
```

- Modify hive-site.xml

```shell
$cd /usr/local/hive/conf
$sudo cp hive-default.xml.template hive-site.xml
$sudo gedit /usr/local/hive/conf/hive-site.xml
```

Add following lines in the configure section at bottom.

```xml
<property>
    <name>system:java.io.tmpdir</name>
    <value>/usr/local/hive/tmp</value>
</property>
```

If you want to change the metastore directory in HDFS, you can modify the value of **hive.metastore.warehouse.dir**

```xml
<property>
  <name>hive.metastore.warehouse.dir</name>
  <value>Your_Path_HERE</value>
  <description>location of default database for the warehouse</description>
</property>
```

- Initialization metastore

```shell
$cd /usr/local/hive/bin/
$schematool -dbType derby -initSchema
```

**If is already have meatstore_db, let original metastore rename be able to create new metastore.**

```shell
$cd /usr/local/hive/bin/
$sudo mv metastore_db metastore_db.tmp
```

- Start Hive

```shell
$cd /usr/local/hive/bin/
$hive
```

---

### Hive Install Step (Remote Mode In Server/Master)

>The installation part is the same as the above steps, the difference is in the hive-site.xml part.

We use mysql to setup hive metastore. Now let's modify hive-site.xml.

```shell
$sudo gedit /usr/local/hive/conf/hive-site.xml
```

- Modify following property.

```xml
<property>
  <name>javax.jdo.option.ConnectionURL</name>
  <value>jdbc:mysql://{ip}:3306/{database is need}?createDatabaseIfNotExist=true</value>
  <description>the URL of the MySQL database</description>
</property>

<property>
  <name>javax.jdo.option.ConnectionDriverName</name>
  <value>com.mysql.jdbc.Driver</value>
  <description>Driver class name for a JDBC metastore</description>
</property>

<property>
  <name>javax.jdo.option.ConnectionUserName</name>
  <value>{mysql_username}</value>
</property>

<property>
  <name>javax.jdo.option.ConnectionPassword</name>
  <value>{mysql_password}</value>
</property>

<property>
 <name>hive.metastore.warehouse.dir</name>
 <value>/usr/hive/warehouse</value>
</property>

<property>
<name>hive.exec.scratchdir</name>
<value>/usr/hive/tmp</value>
</property>

<property>
<name>hive.querylog.location</name>
<value>/usr/hive/log</value>
</property>
```

- Download or copy mysql connecter to hive.

```shell
$wget https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.20/mysql-connector-java-8.0.20.jar
$cp mysql-connector-java-8.0.20 /usr/local/hive/lib
```

- Initialization metastore

```shell
$cd /usr/local/hive/bin/
$schematool --dbType mysql --initSchema
```

### Hive Install Step (Remote Mode In Client/Slave)

- In master, copy hive directory to every slave.

```shell
$sudo scp -r /usr/local/hive username@IP:/tmp
```

- In slave, modify permission and .bashrc

```shell
$sudo mv /tmp/hive /usr/local/
$sudo chmod -R 777 /usr/local/hive/
$sudo gedit .bashrc
```

Add following lines at bottom

```xml
# Set HIVE_HOME
export HIVE_HOME=/usr/local/hive
export PATH=$PATH:$HIVE_HOME/bin
```

Application .bashrc

```shell
$source ~/.bashrc
```

- Modify hive-site.xml

```shell
$sudo gedit /usr/local/hive/conf/hive-site.xml
```

Modify parameter

```xml
<property>
    <name>hive.metastore.uris</name>
    <value>thrift://{server_hostname}:9083</value>
    <description>Thrift URI for the remote metastore. Used by metastore client to connect to remote metastore.</description>
</property>
```

- Start Hive in master

```shell
$cd /usr/local/hive/bin/
$hive --service metastore
```

- Use Hive in slave

```shell
$cd /usr/local/hive/bin/
$hive
```

### Problem Sloved

---

- **java.lang.NoSuchMethodError:com.google.common.base.Preconditions.checkArgument**
  
>This message talk the 'guava' package version is not consistent between hadoop and hive. So we have to know which guava version is older and remove it, copy the least version to another directory.

1. Check guava version in hadoop

```shell
$cd /usr/local/hadoop/share/hadoop/common/lib/
$ls
$cd /usr/local/hive/lib/
$ls
```

In this time, we found guava version is older in hive, so we remove guava in hive, and copy guava from hadoop directory to hive.

2. Remove guava

```shell
$sudo rm /usr/local/hive/lib/guava-19.0.jar
$sudo cp /usr/local/hadoop/share/hadoop/common/lib/guava-27.0-jre.jar /usr/local/hive/lib/
```

- **java.lang.runtimeexception com.ctc.wstx.exc.wstxparsexpection illegal**

Find where it points

>The error message might be like:
[3215,96,"file:/usr/local/hive/conf/hive-site.xml"]

According this message it's means 96th character on line 3215 have illegal character in hive-site.xml. So we delete those character will sloved this problem.

- **java.lang.runtimeexception java.io.IOException: unable to create directory**

>This error message means that it need the directory in hdfs system and local system. Also have permission in both system.

1. Check hdfs directory

```shell
$hadoop fs -ls /usr/local/hive/
```

Observed that permission should be 'drwxrwxr-x' of this directory, if not please do the following line.

```shell
$hadoop fs -chmod g+w /usr/local/hive/{directory name}
```

Repeat the above those step for each layer of the path.

2. Check local directory

First thing is check local system have this directory, if not please do the following line.

```shell
$sudo mkdir /usr/local/hive/tmp
$sudo mkdir /usr/local/hive/warehouse
```

Check directory permission

```shell
$stat {problem directory path}
```

Observed that permission should be 'drwxrwxrwx' of this directory, if not please do the following line.

```shell
$sudo chmod -R 777 {problem directory path}
```

- Failed with exception java.io.IOException:java.lang.IllegalArgumentException: java.net.URISyntaxException: Relative path in absolute URI: ${system:user.name%7D

Modify hive-site.xml can sloved this problem

```shell
$sudo gedit /usr/local/hive/conf/hive-site.xml
```

Modify the value of **hive.exec.local.scratchdir**.
Original at the end of value is ${system:user.name}.
Now we remove the "system:", only left "user.name".
The result is like following snippet.

```xml
<property>
  <name>hive.exec.local.scratchdir</name>
  <value>/home/lch/software/Hive/apache-hive-2.1.1-bin/tmp/${user.name}</value>
  <description>Local scratch space for Hive jobs</description>
</property>
```
