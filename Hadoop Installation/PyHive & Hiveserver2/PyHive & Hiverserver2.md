# PyHive & Hiveserver2 Installation

- Pip install

```shell
$sudo apt-get update
$sudo apt-get install libsasl2-dev
$pip3 install sasl
$pip3 install thrift
$pip3 install thrift-sasl
$pip3 install PyHive
```

- Modify core-site.xml

```shell
$sudo gedit /usr/local/hadoop/etc/hadoop/core-site.xml
```

Add following lines at bottom

```xml
<property>
    <name>hadoop.proxyuser.{user.name}.hosts</name>
    <value>*</value>
</property>
<property>
    <name>hadoop.proxyuser.{user.name}.groups</name>
    <value>*</value>
</property>
```

- Modify hive-site.xml

```shell
$sudo gedit /usr/local/hive/conf/hive-site.xml
```

Modify value of **hive.server2.authentication**.
The value of hive.server2.authentication should become the following after modification

```xml
<property>
    <name>hive.server2.authentication</name>
    <value>NOSAL</value>
</property>
```

- Start Hiveserver2

```shell
$cd /usr/local/hive/bin/
$hiveserver2
```

Waiting all the nodes show up the **Hive Session ID**.

## Problem solved

- command 'x86_64-linux-gnu-gcc' failed with exit status

    You can use following command to solved this problem.

    If your python version is 3, do following command.

    ```shell
    $sudo apt-get install python3-dev
    ```

    If your python version is 2, do following command.

    ```shell
    $sudo apt-get install python2-dev
    ```

    If you want to install specific version of python, do following command.

    ```shell
    $sudo apt-get install python3.x-dev
    ```

- If you install python-dev have "E: Could not get lock /var/lib/dpkg/lock", please do the following command.

    Run these commands one by one.

    ```shell
    $sudo lsof /var/lib/dpkg/lock
    $sudo lsof /var/lib/apt/lists/lock
    $sudo lsof /var/cache/apt/archives/lock
    ```

    And observe the PID, if show the processID of process holding.
    Now we kill the process and type the PID.

    ```shell
    $sudo kill -9 <process_id>
    ```

    You can now safely remove the lock files using the commands below.

    ```shell
    $sudo rm /var/lib/apt/lists/lock
    $sudo rm /var/cache/apt/archives/lock
    $sudo rm /var/lib/dpkg/lock
    ```

    After that, reconfigure the packages.

    ```shell
    $sudo dpkg --configure -a
    ```
