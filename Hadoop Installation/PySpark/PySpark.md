# PySpark Installation

```shell
$pip3 install pysaprk
$sudo gedit /home/{username}/.bashrc
```

Add following lines at bottom

```xml
# PYSPARK_HOME
export PYSPARK_PYTHON=python3
```

Application bashrc file

```shell
$source /home/{username}/.bashrc
```

Open the terminal and type following command line

```shell
$pyspark
```

Check pyspark is running, in pyspark cli type

```pyspark
>>>sc.uiWebUrl
```

Open website and type above url, then check right slide will show "pysparkshell"
