# HBase Installation

## Installation Step

- Download Hbase

    ```shell
    $sudo wget http://ftp.tc.edu.tw/pub/Apache/hbase/2.2.5/hbase-2.2.5-bin.tar.gz
    ```

- Tar file

    ```shell
    $sudo tar xzf hbase-2.2.5-bin.tar.gz
    ```

- Move, Rename, Permission

    ```shell
    $sudo mv hbase-2.2.5 hbase
    $sudo mv hbase /usr/local/
    $sudo chmod -R 777 /usr/local/hbase
    ```

- Modify bashrc file

    ```shell
    $sudo gedit /home/{username}/.bashrc
    ```

    Add following lines at bottom

    ```xml
    #Set HBASE_HOME
    export HBASE_HOME=/usr/local/hbase
    export PATH=$PATH:$HBASE_HOME/bin
    ```

    Application bashrc and check

    ```shell
    $source ~/.bashrc
    $echo $HBASE_HOME
    $echo $PATH
    ```

## Fully Distributed Install Step

- Modify hbase-env.sh

    ```shell
    $sudo gedit /usr/local/hbase/conf/hbase-env.sh
    ```

    Add following line at bottom.

    ```xml
    export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
    export HBASE_MANAGES_ZK=false
    ```

- Modify hbase-site.xml

    ```shell
    $sudo gedit /usr/local/hbase/conf/hbase-site.xml
    ```

    Add following lines at bottom.

    ```xml
    <property>
        <name>hbase.cluster.distributed</name>
        <value>true</value>
    </property>
    <property>
        <name>hbase.rootdir</name>
        <value>hdfs://master:9000/hbase</value>
        <description>pointing to the address of your HDFS instance, using the hdfs:// URI syntax.</description>
    </property>
    <property>
        <name>hbase.zookeeper.quorum</name>
        <value>master,slave01,slave02</value>
    </property>
    <property>
        <name>hbase.zookeeper.property.dataDir</name>
        <value>/usr/local/zookeeper/data</value>
    </property>
    ```

- Modify regionservers

    ```shell
    $sudo gedit /usr/local/hbase/conf/regionservers
    ```

    Remove the line which contain localhost.
    Add line with the hostnames or IP address for cluster.
    It's be like:

    ```xml
    slave01
    slave02
    etc..
    ```

- Create backup master configure

    ```shell
    $sudo touch /usr/local/hbase/conf/backup-master
    $sudo gedit /usr/local/hbase/conf/backup-master
    ```

    Add hostname of cluster in backup-master file, like following lines.

    ```xml
    slave01
    slave02
    etc..
    ```

- Copy whole conf directory of Master to Slave

    ```shell
    $sudo scp -r {source_folder}  {user@host.com:/tmp}
    ```

    **Switch Slave**

    ```shell
    $sudo mv /tmp/hbase /usr/local/
    $sudo chmod -R 777 /usr/local/hbase/
    ```

- Start HBase
    We should start **Hadoop** and **Zookeeper** before start Hbase.

    ```shell
    $cd /usr/local/hadoop/sbin
    $start-all.sh
    ```

    Every machine should start zookeeper, so that can be fully started.

    ```shell
    $cd /usr/local/zookeeper/bin
    $./zkServer.sh start
    ```

    Now it's time to start hbase.

    ```shell
    $cd /usr/local/hbase/bin/
    $./start-hbase.sh
    ```

- Check configured correctly
  
  ```shell
  jps
  ```

  In master machine should show the following service

  ```shell
  HMaster
  ```

  In slave machine should show the following service

  ```shell
  HRegionServer
  ```

- Check the Hbase directory in HDFS

    ```shell
    $cd /usr/local/hadoop/bin
    $hadoop fs -ls /hbase
    ```
