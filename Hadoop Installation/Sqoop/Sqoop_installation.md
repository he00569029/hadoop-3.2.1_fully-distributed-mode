# Sqoop Installation

## Sqoop Download and modify bashrc

```shell
$wget http://ftp.twaren.net/Unix/Web/apache/sqoop/1.4.7/sqoop-1.4.7.tar.gz
$sudo tar xzf sqoop-1.4.7.tar.gz
$sudo mv sqoop-1.4.7 sqoop
$sudo mv sqoop /usr/local/
$sudo gedit .bashrc
```

Add following lines at bottom

```xml
#Set SQOOP_HOME
export SQOOP_HOME=/usr/local/sqoop
export PATH=$PATH:$SQOOP_HOME/bin
export HCAT_HOME=/usr/local/hive
export PATH=$PATH:$HCAT_HOME/hcatalog
export ACCUMULO_HOME=/usr/local/hive
export PATH=$PATH:$ACCUMULO_HOME
```

## Modify sqoop-env.sh

```shell
$cd /usr/local/sqoop/conf/
$cp sqoop-env-template.sh sqoop-env.sh
$sudo gedit /usr/local/sqoop/conf/sqoop-env.sh
```

**Append all the export var where hadoop,hive,hbase,zookeeper path.**

## Download the required jar files

- Mysql-connector-java-8.0.20.jar

    ```shell
    $wget http://ftp.ntu.edu.tw/MySQL/Downloads/Connector-J/mysql-connector-java-8.0.20.tar.gz
    $sudo tar xzf mysql-connector-java-8.0.20.tar.gz
    $cd mysql-connector-java-8.0.20
    $cp mysql-connector-java-8.0.20.jar /usr/local/sqoop/lib/
    $cp mysql-connector-java-8.0.20.jar /usr/local/hadoop/share/hadoop/yarn
    ```

- Sqoop-1.4.7.jar

    ```shell
    $wget http://ftp.twaren.net/Unix/Web/apache/sqoop/1.4.7/sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz
    $sudo tar xzf sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz
    $cd sqoop-1.4.7.bin__hadoop-2.6.0
    $cp sqoop-1.4.7.jar /usr/local/sqoop/lib
    ```

- Commons-lang-2.6.jar

    ```shell
    $wget http://mirrors.tuna.tsinghua.edu.cn/apache//commons/lang/binaries/commons-lang-2.6-bin.zip
    $sudo unzip commons-lang-2.6-bin.zip
    $cd commons-lang-2.6
    $cp commons-lang-2.6.jar /usr/local/sqoop/lib/
    ```

- avro-1.8.2.jar

1. Open website and link to https://jar-download.com/artifacts/org.apache.avro/avro/1.8.2/source-code
2. Download the avro-1.8.2.jar file
3. Do the following command lines.

    ```shell
    $sudo unzip jar_files.zip
    $cd jar_files
    $cp avro-1.8.2.jar /usr/local/sqoop/lib/
    ```
