# Zookeeper_Installation

## Install

- Download Zookeeper

```shell
$sudo wget https://downloads.apache.org/zookeeper/zookeeper-3.6.1/apache-zookeeper-3.6.1-bin.tar.gz
```

- Tar File

```shell
$sudo tar xzf apache-zookeeper-3.6.1-bin.tar.gz
```

- Move, Rename, Permission

```shell
$sudo mv apache-zookeeper-3.6.1-bin zookeeper
$sudo mv zookeeper /usr/local/
$sudo chmod -R 777 /usr/local/zookeeper
```

## Configuration

- Modify .bashrc file
  
```shell
$cd /home/{user_name}
$sudo gedit .bashrc
```

Add following lines at bottom.

```xml
export ZOO_HOME=/usr/local/zookeeper
export PATH=$PATH:$ZOO_HOME/bin
```

Apply .bashrc and check

```shell
$source ~/.bashrc
$echo $ZOO_HOME
$echo $PATH
```

- Zoo.cfg

```shell
$sudo touch /usr/local/zookeeper/conf/zoo.cfg
$sudo gedit /usr/local/zookeeper/conf/zoo.cfg
```

Add following lines

```xml
tickTime=2000
dataDir=/usr/local/zookeeper/data
clientPort=2181
initLimit=5
syncLimit=2
server.1=master:2888:3888
server.2=slave01:2888:3888
server.3=slave02:2888:3888
```

>**The server.1 or 2 or 3 is correspond to value of myid file in /zookeeper/{dataDir}**

- Myid file

```shell
$sudo mkdir /usr/local/zookeeper/data
$sudo touch /usr/local/zookeeper/data/myid
$sudo gedit /usr/local/zookeeper/data/myid
```

Enter a number, which repersents the machine.
This value will be related to zoo.cfg.
In Master enter

```xml
1
```

In Slave01 enter

```xml
2
```

In Slave02 enter

```xml
3
```

We have to copy zookeeper folder to every slave machine before modify myid value in slave.

```shell
$sudo scp -r /usr/local/zookeeper username@IP:/tmp
```

Switch slave machine

```shell
$sudo mv /tmp/zookeeper /usr/local/
$sudo chmod -R 777 /usr/local/zookeeper
```

**Now we can modify value of myid file.**

- Start Zookeeper

>Start zookeeper for every machine. There is no difference in the order of startup

Now we start in the order of Master, Slave01, Slave02.

```shell
$cd /usr/local/zookeeper/bin
$./zkServer.sh start
```

After all machine have entered the above command, enter the following command in Master.

```shell
$./zkCli.sh
```

## Implementation Issue

The first thing is check log file in /usr/local/zookeeper/log/, and find the log file of this machine name.

- Cannot open channel to X at election address 3888..
  Two slove way.
  1. Modify zoo.cfg
     In Master

     ```xml
      server.1=0.0.0.0:2888:3888
      server.2=slave01:2888:3888
      server.3=slave02:2888:3888
      ```

     In Slave01

     ```xml
      server.1=master:2888:3888
      server.2=0.0.0.0:2888:3888
      server.3=slave02:2888:3888
      ```

     In Slave02

     ```xml
      server.1=master:2888:3888
      server.2=slave01:2888:3888
      server.3=0.0.0.0:2888:3888
      ```

  2. On the machine with error message, do the following command.

    ```shell
    $cd /usr/local/zookeeper/bin
    $./zkServer.sh restart
    ```

    This reason is because the position of the second and third stations cannot be captured when the first station is executed. So we have to restart the machine with error message.
